#Title: Turing Assembler
#Purpose: Convert assembly into binary to be used with the Interpreter
#Language: Python
#Compiler: Python 3.4
#Compile: g++ turasm.cpp -o turasm
#Run: ./turasm asm-file

#Author: Jordan Wagner
#Created for class: CS441 - Computer System Architecture
#Professor of class: William Confer
#School: State University of New York Polytechnic Institute
#Date: 10/16/2015
#Assembler
import sys
import re

#Define my functions
#Convert Ascii to Binary padded to specified size using leading zeros
def iter_bin(s,size):
	sb = s.encode('ascii')
	converted = (format(b, '07b') for b in sb)
	for s in converted:
		formated = "{:0>{size}}".format(s, size=size)
	#formated = "{:0>8}".format(converted)
	return formated

#Stop command
def Stop(IsHalt, BinFile, Line):
	Errors = []
	if not IsHalt:
		Errors.append("Line "+str(Line)+": Stop command - Expected a 1 or 0 to follow the command. 1 indicates to Halt and 0 indicates to stop.")
	elif IsHalt and len(IsHalt) > 2 and IsHalt.strip()[1:].strip()[0] != '#':
		Errors.append("Line "+str(Line)+": Stop command - Unexpected character after command.")
	elif IsHalt and IsHalt.strip()[0] != '1' and IsHalt.strip()[0] != '0':
		Errors.append("Line "+str(Line)+": Stop command - Unexpected character after command. The first character after the command should be either a 1 or a 0.")
	else:
		BinFile.write("1100000"+IsHalt.strip()[0]+"00000000")
	return Errors

#HaltOrFail command
def HaltOrFail(IsHalt, afterCom, BinFile, Line):
	Errors = []
	type = ("Halt" if IsHalt == 1 else "Fail")
	if not afterCom:
		Stop(str(IsHalt), BinFile, Line)
	elif afterCom and afterCom.strip()[0] != '#':
		Errors.append("Line "+str(Line)+": "+type+" command - Unexpected character after command. Nothing other then a comment should follow this command.")
	else:
		Stop(str(IsHalt), BinFile, Line)
	return Errors

#Move command - No longer supported. It does not support 2 bit. All commands must be 2 bit.
def Move(Line):
	Errors = []
	Errors.append("Line "+str(Line)+": Move command - The move command is no longer supported. The commands must all be 2 bits and Move cannot support that. Please use either the, Left [count] or Right [count], command.")
	return Errors

#Erase command
def Erase(file, command, BinFile, Line):
	Errors = []
	if command and command.strip() and command.strip()[0] != '#':
		Errors.append("Line "+str(Line)+": Erase command - Unexpected character after command.")
	else:
		pos = file.tell()
		next = file.readline()
		if next and next.lower().startswith("right"):
			result = LeftTRightF(next, 0, Line+1)
			if result[0] != 0:
				BinFile.write("111")
				BinFile.write(result[0])
				BinFile.write("000000000")
			Error = Errors + result[1]
		elif next and next.lower().startswith("left"):
			result = LeftTRightF(next, 1, Line+1)
			if result[0] != 0:
				BinFile.write("111")
				BinFile.write(result[0])
				BinFile.write("100000000")
			Error = Errors + result[1]
		else:
			BinFile.write("1110000000000000")
			file.seek(pos)
	return Errors

#Left and Right commands. Does error checking and returns appopriate number of characters to move.
#This function should be called by assembly specific command functions. It is used to return the 
#number of moves and the errors. This function will not write the binary for Left or Right to the
#binary file.
def LeftTRightF(command, IsLeft, Line):
	Errors = []
	count = 0
	if IsLeft:
		afterMove = command[4:].strip()
	else:
		afterMove = command[5:].strip()
	match = re.match(r"([0-9]+)([^0-9]+)", afterMove, re.I)
	if match and match.group(2).strip() and match.group(2).strip()[0] != '#':
		Errors.append("Line "+str(Line)+": "+("Left" if IsLeft else "Right")+" command - Unexpected character after count.")
	elif afterMove and afterMove.split("#")[0].strip().isdigit():
		count = '{0:04b}'.format(int(afterMove.split("#")[0].strip().split("#")[0].strip()))
	else:
		Errors.append("Line "+str(Line)+": "+("Left" if IsLeft else "Right")+" command - Expected an integer to follow the command.")
	return [count,Errors]

#Left and Right commands. Does not do error checking. It sends it to the LeftTRightF function for that.
#This function should be called by programmer via main or other non-command converting functions. This
#function is used only to return the appropriate data to outside functions. 
def LeftRight(command, IsLeft, Line):
	results = LeftTRightF(command, IsLeft, Line)
	BinFile.write("101"+str(results[0])+str(IsLeft)+"00000000")
	return results[1]

#Write characters in draw command to binary
def DrawToBinary(file, characters, BinFile, Line):
	Errors = []
	if characters and characters[0] == '\'':
		if characters and len(characters) >= 2 and characters[1] == '\'':
			Errors = Errors + Erase(file, characters[2:].strip(), BinFile, Line)
		elif characters and len(characters) >= 3 and characters[2] != '\'':
			Errors.append("Line "+str(Line)+": Draw command - Expected one comparing character with an apostrophe (') after.")
		elif characters and len(characters) > 3 and characters[3:].strip()[0] != '#':
			Errors.append("Line "+str(Line)+": Draw command - Unexpected character after second apostrophe (').")
		elif characters and len(characters) >= 3 and characters[2] == '\'':
			pos = file.tell()
			next = file.readline()
			if next and next.lower().startswith("right"):
				result = LeftTRightF(next, 0, Line+1)
				if result[0] != 0:
					BinFile.write("100")
					BinFile.write(result[0])
					BinFile.write("0")
					BinFile.write(iter_bin(characters[1],8))
				Errors = Errors + result[1]
			elif next and next.lower().startswith("left"):
				result = LeftTRightF(next, 1, Line+1)
				if result[0] != 0:
					BinFile.write("100")
					BinFile.write(result[0])
					BinFile.write("1")
					BinFile.write(iter_bin(characters[1],8))
				Errors = Errors + result[1]
			else:
				BinFile.write("10000000")
				BinFile.write(iter_bin(characters[1],8))
				file.seek(pos)
		elif characters and ((len(characters) >= 3 and characters[2] != '\'') or (len(characters) >= 2 and characters[1] != '\'') or (len(characters) == 1 )):
			Errors.append("Line "+str(Line)+": Draw command - Expected an apostrophe (') either one or two characters after the first but did not find one.")
		else:
			Errors.append("Line "+str(Line)+": Draw command - Unexpected error occured. No definition, source or reason for this error.")
	elif characters and characters[0] != '\'':
		Errors.append("Line "+str(Line)+": Draw command - Expected first character after the command to be an apostrophe (') or quotation mark (\").")
	elif characters and characters[0] == '"':
		Errors.append("Line "+str(Line)+": Draw command - First character after command should be an apostrophe (') but a quotation mark (\") was found. The draw command cannot accept multiple characters at once.")
	elif characters and ((len(characters) >= 3 and characters[2] != '\'') or (len(characters) >= 2 and characters[1] != '\'') or (len(characters) == 1 )):
		Errors.append("Line "+str(Line)+": Draw command - Expected an apostrophe (') either one or two characters after the first but did not find one.")
	elif not characters:
		Errors.append("Line "+str(Line)+": Draw command - Expected two quotation marks and an optional character of the command.")
	else:
		Errors.append("Line "+str(Line)+": Draw command - Unexpected error occured. No definition, source or reason for this error.")
	return Errors


#Write characters in alphabet command to binary
def AlphaToBinary(characters, BinFile, Line):
	Errors = []
	#for loc in range(len(characters)):
	if characters and characters[0] == '\'':
		if len(characters) < 3 or characters[2] != '\'':
			Errors.append("Line "+str(Line)+": Alpha command - Found 1 apostrophe ('). Expected a second within 2 characters but did not find one.")
		elif len(characters) > 3 and characters[3:].strip()[0] != '#':
			Errors.append("Line "+str(Line)+": Alpha command - Unnexpected character after second apostrophe (').")
		BinFile.write("00000000")#add char to alphabet
		BinFile.write(iter_bin(characters[1],8))
		return Errors#escape loop for current line in asm file
	elif characters and characters[0] == '"':
		for loc in range(1, len(characters)):
			if characters[loc] == '"':
				if len(characters) > loc+1 and characters[loc+1:].strip()[0] != '#':
					Errors.append("Line "+str(Line)+": Alpha command - Unnexpected character after second quotation mark (\").")
				return Errors
			elif loc < len(characters):
				BinFile.write("00000000")
				BinFile.write(iter_bin(characters[loc],8))
			if loc+1 >= len(characters) and characters[loc+1] != '"':
				Errors.append("Line "+str(Line)+": Alpha command - Found 1 quotation mark. Expected a second quotation mark (\") on the same line but did not find one.")
				return Errors
	else:
		Errors.append("Line "+str(Line)+": Alpha command - Expected a quotation mark (\") or apostrophe (') after alpha but did not find one.")
	return Errors

#Write character in CMP to binary
def CmpToBinary(characters, BinFile, Line):
	Errors = []
	if characters and characters[0] == '\'':
		if characters and len(characters) >= 2 and characters[1] == '\'':
			if len(characters) > 2 and characters[2:].strip()[0] != '#':
				Errors.append("Line "+str(Line)+": Cmp command - Unexpected character after second apostrophe (').")
			BinFile.write("0010000100000000")
			return Errors
		elif characters and len(characters) >= 3 and characters[2] != '\'':
			Errors.append("Line "+str(Line)+": Cmp command - Expected one comparing character with an apostrophe (') after.")
		elif characters and len(characters) > 3 and characters[3:].strip()[0] != '#':
			Errors.append("Line "+str(Line)+": Cmp command - Unexpected character after second apostrophe (').")
		BinFile.write("00100000")
		BinFile.write(iter_bin(characters[1],8))
	elif characters and characters[0] == '"':
		if characters and len(characters) >= 2 and characters[1] == '"':
			if len(characters) > 2 and characters[2:].strip()[0] != '#':
				Errors.append("Line "+str(Line)+": Cmp command - Unexpected character after second quotation mark (\").")
			BinFile.write("0010000100000000")
			return Errors
		elif characters and len(characters) >= 3 and characters[2] != '"':
			Errors.append("Line "+str(Line)+": Cmp command - Expected one comparing character with a quotation mark (\") after.")
		elif characters and len(characters) > 3 and characters[3:].strip()[0] != '#':
			Errors.append("Line "+str(Line)+": Cmp command - Unexpected character after second quotation mark (\").")
		BinFile.write("00100000")
		BinFile.write(iter_bin(characters[1],8))
	else:
		Errors.append("Line "+str(Line)+": Cmp command - Expected first character after the command to be an apostrophe (') or quotation mark.")
	return Errors

#Writes binary for all branching commands
def Branching(afterCom, BinFile, Line, type, LabelTable):
	Errors = []
	Label = re.split('#| ', afterCom[0:])[0]
	if type == 2:
		command = "BRANE"
	elif type == 1:
		command = "BRAE"
	else:
		command = "BRA"
	if not afterCom:
		Errors.append("Line "+str(Line)+": "+command+" command - Expected label after command.")
	elif afterCom[0] != '!':
		Errors.append("Line "+str(Line)+": "+command+" command - Expected first character after command to be an exlimation mark (!) to indicate the label name.")
	elif len(afterCom) > len(Label) and afterCom[len(Label):].strip()[0] != '#':
		Errors.append("Line "+str(Line)+": "+command+" command - Unexpected character(s) after label name.")
	else:
		checkExists = [x for x in LabelTable if Label[1:] in x[1] and len(x[1]) == len(Label[1:])]
		if checkExists:
			if type==0 or type==1:
				BinFile.write("011")
				BinFile.write('{0:013b}'.format(checkExists[0][0]))
			if type==0 or type==2:
				BinFile.write("010")
				BinFile.write('{0:013b}'.format(checkExists[0][0]))
		else:
			Errors.append("Line "+str(Line)+": "+command+" command - Label not found, "+Label+".")
	return Errors

#Used to count the characters in the alpha double quote command. Useful for GetLabelTable function
def CountCharractersInAlpha(afterCom):
	count = 0
	for loc in range(1, len(afterCom)):
		if afterCom[loc] == '"':
			if len(afterCom) > loc+1 and afterCom[loc+1:].strip()[0] != '#':
				return count#we reached this because of error
			return count#this was not an error
		elif loc < len(afterCom):
			count = count + 2
		if loc+1 >= len(afterCom) and afterCom[loc+1] != '"':
			return count
	return count

#Creates and returns Label Table
def GetLabelTable(AsmFile):
	#Create Variables
	LabelTable = []
	Address = 0
	LabelElement = 0
	#Loop through file
	AsmFile = open(AsmFile,'r')
	with AsmFile as file:
		#for fileLine in file:
		for Line, fileLine in enumerate(iter(file.readline, '')):
			#fileLine = file.readline()
			fileLine = fileLine.strip()
			if fileLine and fileLine[0]=='!':
				LabelTable.append([])
				LabelName = re.split('#| ',fileLine[1:])[0]
				LabelTable[LabelElement].append(Address)
				LabelTable[LabelElement].append(LabelName)
				LabelElement = LabelElement + 1
			elif fileLine and fileLine[0] != '#':
				if fileLine.lower().startswith("alpha"):
					afterCom = fileLine[5:].strip()
					if afterCom and afterCom[0] == '\'':
						Address = Address + 2
					elif afterCom and afterCom[0] == '"':
						Address = Address + CountCharractersInAlpha(afterCom)
				elif not fileLine.lower().startswith("brae") and not fileLine.lower().startswith("brane") and fileLine.lower().startswith("bra"):
					Address = Address + 4
				elif fileLine and (fileLine.lower().startswith("draw") or fileLine.lower().startswith("erase")):
					pos = file.tell()
					next = file.readline()
					if next and (next.lower().startswith("right") or next and next.lower().startswith("left")):
						Address = Address + 2
					else:
						file.seek(pos)
						Address = Address + 2
				else:
					Address = Address + 2
	return LabelTable

#Converts commands in assembly file (Param1) to Binary storing it in binary file (Param2)
def CommandSearch(AsmFile, BinFile, LabelTable):
	#Create Variables
	Errors = []
	#Loop through file
	AsmFile = open(AsmFile,'r')
	with AsmFile as file:
		#for Line, fileLine in enumerate(file):
		for Line, fileLine in enumerate(iter(file.readline, '')):
			Line = Line+1
			fileLine = fileLine.strip()
			if fileLine and fileLine[0] != '#' and fileLine[0] != '!':
				if fileLine.lower().startswith("alpha"):
					afterCom = fileLine[5:].strip()
					Errors = Errors + AlphaToBinary(afterCom, BinFile, Line)
				elif fileLine.lower().startswith("cmp"):
					afterCom = fileLine[3:].strip()
					Errors = Errors + CmpToBinary(afterCom, BinFile, Line)
				elif fileLine.lower().startswith("brane"):
					afterCom = fileLine[5:].strip()
					Errors = Errors + Branching(afterCom, BinFile, Line, 2, LabelTable)
				elif fileLine.lower().startswith("brae"):
					afterCom = fileLine[4:].strip()
					Errors = Errors + Branching(afterCom, BinFile, Line, 1, LabelTable)
				elif fileLine.lower().startswith("bra"):
					afterCom = fileLine[3:].strip()
					Errors = Errors + Branching(afterCom, BinFile, Line, 0, LabelTable)
				elif fileLine.lower().startswith("draw"):
					afterCom = fileLine[4:].strip()
					Errors = Errors + DrawToBinary(file, afterCom, BinFile, Line)
				elif fileLine.lower().startswith("move"):
					Errors = Errors + Move(Line)
				elif fileLine.lower().startswith("stop"):
					afterCom = fileLine[4:].strip()
					Errors = Errors + Stop(afterCom, BinFile, Line)
				elif fileLine.lower().startswith("halt"):
					afterCom = fileLine[4:].strip()
					Errors = Errors + HaltOrFail(1, afterCom, BinFile, Line)
				elif fileLine.lower().startswith("fail"):
					afterCom = fileLine[4:].strip()
					Errors = Errors + HaltOrFail(0, afterCom, BinFile, Line)
				elif fileLine.lower().startswith("erase"):
					afterCom = fileLine[5:].strip()
					Errors = Errors + Erase(file, afterCom, BinFile, Line)
				elif fileLine.lower().startswith("left"):
					afterCom = fileLine[4:].strip()
					Errors = Errors + LeftRight(fileLine, 1, Line)
				elif fileLine.lower().startswith("right"):
					afterCom = fileLine[5:].strip()
					Errors = Errors + LeftRight(fileLine, 0, Line)
				else:
					Errors.append("Line "+str(Line)+": Command not found.")
	BinFile.seek(0)
	return Errors


def PrintErrors(Errors):
	i = 0
	for error in Errors:
		print('Error',str(i)+':',error)
		i = i+1

#main
#Open/Create Files
BinFile = open("Binary.txt",'w')
AsmFile = "Assembly.txt"
LabelTable = GetLabelTable(AsmFile)
Errors = CommandSearch(AsmFile, BinFile, LabelTable)
BinFile.close()
if Errors:
	print("Failed: There was an error converting the assembly.\n")
	for err in Errors:
		print(err)
else:
	print("Success: The assembly was converted without errors.")